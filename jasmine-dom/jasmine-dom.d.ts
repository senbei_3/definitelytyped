// Type definitions for Jasmine-dom
// Project: http://github.com/jeffwatkins/jasmine-dom
// Definitions by:
// DefinitelyTyped:

declare function readFixtures(...fixtureUrls:string[]):void;
declare function loadFixtures(...fixtureUrls:string[]):void;
declare function setFixtures(html:HTMLElement):void;
declare function sandbox(attributes:string):HTMLElement;    // Must Check
declare function sandbox(attributes:any[]):HTMLElement;  // Must Check

declare module jasmine {
    function getFixtures():Fixtures;

    interface Fixtures {
        new ();
        set(html:HTMLElement):void;
        load():void;
        read():HTMLElement;
        clearCache():void;
        cleanUp():void;
        sandbox(attributes:string):HTMLElement; // Must Check
        sandbox(attributes:any[]):HTMLElement; // Must Check
    }

    interface Matchers {
        toHaveClass(className:string):boolean;
        toBeVisible():boolean;
        toBeHidden():boolean;
        toBeSelected():boolean;
        toBeChecked():boolean;
        toBeEmpty():boolean;
        toExist():boolean;
        toHaveAttr(attributeName:string, expectedAttributeValue:any):boolean;
        toHaveId(id:string):boolean;
        toHaveHtml(html:string):boolean;
        toHaveText(text:string):boolean;
        toHaveValue(value:string):boolean;
        toMatchSelector(selector:string):boolean;
        toContain(selector:string):boolean;
    }
}
